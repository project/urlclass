README
------

Simple filter for Drupal.
Adds a CSS class to all <a>-elements reflecting their target.

Example:
<a href="http://google.de">Google</a> becomes <a href="http://google.de" class="google">Google</a>

Sponsored by 
erdfisch :: internetlösungen http://erdfisch.de

Comments and suggestions to
Stefan Auditor <stefan.auditor@erdfisch.de>

Please report any bugs on
http://drupal.org/project/issues/urlclass